package akka.first.app.actors;

 

 

import akka.actor.UntypedActor;
import akka.first.app.messages.CrawlerJobType;
import akka.first.app.messages.GeneralJobMessage;

/**
 * @author Zoran Jeremic 2013-07-15
 */
public class DefaultWorkerActor  extends UntypedActor {
	public boolean started;
	public DefaultWorkerActor(String param1, String param2){
		started=true;
	}
	@Override
	public void onReceive(Object message) throws Exception {
		if(message instanceof GeneralJobMessage){
			if(((GeneralJobMessage) message).getJobType().equals(CrawlerJobType.STOPCRAWLER)){
				System.out.println("WORKER STOP MESSAGE PROCESSED:"+((GeneralJobMessage) message).getCounter());
				started=false;
				//_parent.tell(new GeneralJobMessage(CrawlerJobType.STOPCRAWLER, "worker_"+((GeneralJobMessage) message).getCounter()), this.getSelf());
			}else{
				if(started){
					String counter=((GeneralJobMessage) message).getCounter();
					System.out.println("WORKER PROCESSED OTHER JOB:"+counter);
				}
				
			}
		}
	}
}
