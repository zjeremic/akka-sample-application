package akka.first.app.actors;


 
import java.util.List;

import java.util.LinkedList;

 

import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.first.app.messages.CrawlerJobType;
import akka.first.app.messages.GeneralJobMessage;

/**
 * @author Zoran Jeremic 2013-07-15
 */
public class CrawlerManager  extends UntypedActor{
	//ActorSystem _system;
	
	List<ActorRef> crawlers=new LinkedList<ActorRef>();
	public CrawlerManager( String param1, String param2){
	 
	}
	public void startJobs(){
		int counter=0;
		
		for (int i=0;i<5;i++){
			 ActorRef crawlerActor= getContext().actorOf(Props.create(DefaultCrawlerActor.class,  "param1","param2")
		     		 .withDispatcher("priorDispatcher"));
			 crawlers.add(crawlerActor);
		}
		while(counter<1000){
			counter++;
			int crawlerId=0;
			for(ActorRef crawler:crawlers){
				crawlerId++;
				String messageId=crawlerId+"."+counter;
				crawler.tell(new GeneralJobMessage(CrawlerJobType.OTHERJOB, "crawler_",messageId), null);
				 
			}
			
		}
	}
		
	 
	 
	@Override
	public void onReceive(Object message) throws Exception {
		if(message instanceof GeneralJobMessage){
			if(((GeneralJobMessage) message).getJobType().equals(CrawlerJobType.STOPCRAWLER)){
				for(ActorRef crawler:crawlers){
					 crawler.tell(new GeneralJobMessage(CrawlerJobType.STOPCRAWLER, "crawler_",((GeneralJobMessage) message).getCounter()), this.getSelf());
				}
			}
		}else if(message instanceof String){
			startJobs();
		}
	}
}
