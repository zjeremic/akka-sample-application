package akka.first.app.actors;

 

 

import java.util.LinkedList;
import java.util.List;

 

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.first.app.messages.CrawlerJobType;
import akka.first.app.messages.GeneralJobMessage;

/**
 * @author Zoran Jeremic 2013-07-15
 */
public class DefaultCrawlerActor  extends UntypedActor {
	public boolean started;
	private List<ActorRef> workers= new LinkedList<ActorRef>();
//	ActorRef _parent;
	public DefaultCrawlerActor(  String param1, String param2){
		started=true;
		//_parent=parent;
		for(int i=0;i<5;i++){
			ActorRef worker= getContext().actorOf(Props.create(DefaultWorkerActor.class,"param1","param2")
		     		 .withDispatcher("priorDispatcher"));
			workers.add(worker);
		}
	}
	@Override
	public void onReceive(Object message) throws Exception {
		if(message instanceof GeneralJobMessage){
			if(((GeneralJobMessage) message).getJobType().equals(CrawlerJobType.STOPCRAWLER)){
				System.out.println("STOP MESSAGE PROCESSED:"+((GeneralJobMessage) message).getCounter());
				started=false;
				getContext().parent().tell(new GeneralJobMessage(CrawlerJobType.STOPCRAWLER, "manager_",
						((GeneralJobMessage) message).getCounter()), this.getSelf());
				
				//int workerId=0;
				 for(ActorRef worker:workers){
					 
					 worker.tell(new GeneralJobMessage(CrawlerJobType.STOPCRAWLER,  "worker_",((GeneralJobMessage) message).getCounter()), this.getSelf());
				 }
			}else{
				if(started){
					System.out.println("CRAWLER PROCESSED OTHER JOB:"+((GeneralJobMessage) message).getCounter());
					String counter=((GeneralJobMessage) message).getCounter();
					int workerId=0;
					 for(ActorRef worker:workers){
						 workerId++;
						 worker.tell(new GeneralJobMessage(CrawlerJobType.OTHERJOB, "worker_",counter+"."+workerId), this.getSelf());
					 }
					 if(counter.contains(".75")){
							System.out.println("EVERYTHING SHOULD STOP HERE......");
							getContext().parent().tell(new GeneralJobMessage(CrawlerJobType.STOPCRAWLER, "manager_",((GeneralJobMessage) message).getCounter()), this.getSelf());
							 workerId=0;
							 for(ActorRef worker:workers){
								 workerId++;
								 worker.tell(new GeneralJobMessage(CrawlerJobType.STOPCRAWLER, "worker_",counter+"."+workerId), this.getSelf());
							 }
						}
				}
				
			}
		}
	}
}
