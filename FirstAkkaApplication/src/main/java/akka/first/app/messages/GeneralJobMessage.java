package akka.first.app.messages;
/**
 * @author Zoran Jeremic 2013-07-15
 */
public class GeneralJobMessage {
	private CrawlerJobType jobType;
	private String counter;
	private String receiver;
	
	public GeneralJobMessage(CrawlerJobType type, String receiver, String count){
		jobType=type;
		counter=count;
	}

	public CrawlerJobType getJobType() {
		// TODO Auto-generated method stub
		return jobType;
	}
	public String getCounter(){
		return counter;
	}
	public String getReceiver(){
		return receiver;
	}

}
